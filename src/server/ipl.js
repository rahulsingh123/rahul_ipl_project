exports.team_Won_Toss_And_Matches = function (matches) {
  const result = {};
  for (let match of matches) {
    if (match.toss_winner === match.winner) {
      if (result[match.toss_winner]) {
        result[match.toss_winner] += 1;
      } else {
        result[match.toss_winner] = 1;
      }
    }
  }
  return result;
};

exports.player_Of_The_Match_Per_Season = function (matches) {
  const final = {};
  const year = [];
  for (let match of matches) {
    if (year.includes(match.season) === false) {
      year.push(match.season);
    }
  }
  for (let i of year) {
    let temp = {};
    for (let match of matches) {
      if (match.season === i) {
        if (temp[match.player_of_match]) {
          temp[match.player_of_match] += 1;
        } else {
          temp[match.player_of_match] = 1;
        }
      }
    }
    let max;
    let k = 0;
    for (let i in temp) {
      if (k < temp[i]) {
        k = temp[i];
        max = i;
      }
    }
    final[i] = max;
  }
  // console.log(final)
  return final;
};

exports.strike_Rate_Of_A_Batsman_In_Each_Season = function (matches, deliveries) {
  const result = {};
  const player = "MS Dhoni";
  const year = [];
  for (let match of matches) {
    if (year.includes(match.season) === false) {
      year.push(match.season);
    }
  }
  for (let i of year) {
    let runs = 0;
    let balls = 0;
    for (let match of matches) {
      if (i === match.season) {
        for (let del of deliveries) {
          if (match.id === del.match_id) {
            if (del.batsman === player) {
              runs += parseInt(del.batsman_runs);
              balls += 1;
            }
          }
        }
      }
    }
    let calc = (runs / balls) * 100;
    result[i] = calc.toFixed(2);
  }
  return result;
};

exports.best_Economy_In_Super_Overs = function (deliveries) {
  const result = {};
  const balls = {};
  const runs = {};
  for (let del of deliveries) {
    let super_Over_Check = parseInt(del.is_super_over);
    if (super_Over_Check) {
      if (balls[del.bowler]) {
        balls[del.bowler] += 1;
      } else {
        balls[del.bowler] = 1;
      }
      if (runs[del.bowler]) {
        let calc =
          parseInt(del.batsman_runs) +
          parseInt(del.noball_runs) +
          parseInt(del.wide_runs);
        runs[del.bowler] += calc;
      } else {
        let calc =
          parseInt(del.batsman_runs) +
          parseInt(del.noball_runs) +
          parseInt(del.wide_runs);
        runs[del.bowler] = calc;
      }
    }
  }
  for (let i in runs) {
    let economy = (runs[i] / (balls[i] / 6)).toFixed(2);
    result[i] = economy;
  }

  let entries = Object.entries(result);
  let sorted = entries.sort((a, b) => {
      return a[1] - b[1]
  })
  const finalData = {}
  for(let i of sorted){
      finalData[i[0]] = i[1]
  }
  return finalData
  
};

exports.player_Dismissed_By_Another_Player = (deliveries) => {
    const result = {}
    const player_Name = "MS Dhoni";
    for(let del of deliveries){
        if(del.player_dismissed == "MS Dhoni"){
            if(result.hasOwnProperty(del.bowler)){
                result[del.bowler] +=1
            }
            else{
                result[del.bowler] = 1
            }
        }

    }
    // console.log(result)
    let check = Object.entries(result);
    let sorted = check.sort((a,b)=>{
        return b[1] - a[1]
    })
    // console.log(sorted)
    const finalData = {}
    for(let i of sorted){
        finalData[i[0]] = i[1]
    }
    // console.log(finalData)
    return finalData

};
